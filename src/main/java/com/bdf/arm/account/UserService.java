package com.bdf.arm.account;

import com.bdf.arm.entity.Account;
import com.bdf.arm.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

public class UserService implements UserDetailsService{

    @Autowired
    private AccountRepository accountRepository;

    @Inject
    private PasswordEncoder passwordEncoder;



    @PostConstruct
    protected void initialize() {
  //      accountRepository.save(new Account("user", "demo", "ROLE_USER"));
    //    accountRepository.save(new Account("admin", "admin", "ROLE_ADMIN"));
      //  accountRepository.save(new Account("cliente", "cliente", "ROLE_CLIENTE"));

    }

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {

        Account account = accountRepository.findByUsername(userName);

        if (account == null) {
            throw new UsernameNotFoundException("user not found");
        }
        return createUser(account);
    }

    public void signin(Account account) {
        SecurityContextHolder.getContext().setAuthentication(authenticate(account));
    }

    private Authentication authenticate(Account account) {
        return new UsernamePasswordAuthenticationToken(createUser(account), null, account.getAuthorities());
    }

    private User createUser(Account account) {
        return new User(account);
    }

    public static class User extends org.springframework.security.core.userdetails.User {

        private final Account account;

        public User(Account account) {
            super(account.getUsername(), account.getPassword(), account.getAuthorities());
            this.account = account;
        }

        public Account getAccount() {
            return account;
        }

        public boolean isAdmin() {
            return getAccount().isAdmin();
        }
    }



}
