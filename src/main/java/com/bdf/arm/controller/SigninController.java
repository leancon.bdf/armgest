package com.bdf.arm.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

@Controller
public class SigninController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SigninController.class);

    @RequestMapping(value = "signin")
    public ModelAndView signin(@RequestParam(value = "error", required = false) String error,
                               HttpServletRequest request) {

        ModelAndView model = new ModelAndView();
        model.setViewName("signin/signin");

        if (error != null) {
            model.addObject("errorMessage", getErrorMessage(request, "SPRING_SECURITY_LAST_EXCEPTION"));
        }

        return model;
    }

    @RequestMapping(value = "logout")
    public String logout(HttpServletRequest request) throws ServletException {

        LOGGER.info("Logout Satisfactorio del usuario: " + request.getUserPrincipal().getName());
        request.logout();
        return "redirect:/";
    }


    // customize the error message
    private String getErrorMessage(HttpServletRequest request, String key) {

        Exception exception = (Exception) request.getSession().getAttribute(key);

        String error = exception.getMessage();
        return error;
    }
}



