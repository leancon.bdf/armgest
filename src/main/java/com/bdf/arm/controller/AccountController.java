package com.bdf.arm.controller;

import com.bdf.arm.entity.Account;
import com.bdf.arm.entity.UserRole;
import com.bdf.arm.repository.AccountRepository;
import com.bdf.arm.repository.UserRoleRepository;
import com.bdf.arm.support.web.AjaxUtils;
import com.bdf.arm.support.web.MessageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Date;
import java.util.List;

@Controller
//@Secured("ROLE_USER")
class AccountController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountController.class);

    @ModelAttribute("page")
    public String module() {
        return "usuarios";
    }

    private static final String SIGNUP_VIEW_NAME = "signup/signup";
    private static final String USERS_VIEW_NAME = "signup/users";


    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

//	@Autowired
//	private UserService userService;

    @RequestMapping(value = "signup")
    public String signup(Model model, @RequestHeader(value = "X-Requested-With", required = false) String requestedWith) {

        SignupForm form = new SignupForm();
        form.setRoles((List<UserRole>) userRoleRepository.findAll());
        model.addAttribute(form);
        if (AjaxUtils.isAjaxRequest(requestedWith)) {
            return SIGNUP_VIEW_NAME.concat(" :: signupForm");
        }
        return SIGNUP_VIEW_NAME;
    }

    @RequestMapping(value = "signup", method = RequestMethod.POST)
    public String signup(@Valid @ModelAttribute SignupForm signupForm, Errors errors, RedirectAttributes ra) {
        if (errors.hasErrors()) {
            signupForm.setRoles((List<UserRole>) userRoleRepository.findAll());
            return SIGNUP_VIEW_NAME;
        }
        Account account = signupForm.createAccount();
        account.setPassword(passwordEncoder.encode(account.getPassword()));
        account.setCreationTime(new Date());
        accountRepository.save(account);
//		userService.signin(account);
        // see /WEB-INF/i18n/messages.properties and /WEB-INF/views/homeSignedIn.html
        MessageHelper.addSuccessAttribute(ra, "signup.success");
        return "redirect:/users";
    }

    @RequestMapping(value = "users")
    public ModelAndView  users() {

        ModelAndView model = new ModelAndView();

        model.addObject(accountRepository.findAll());
        // model.addAttribute();
        model.setViewName(USERS_VIEW_NAME);

        return model;
    }

    @RequestMapping(value = "/account/{operation}/{userId}", method = RequestMethod.GET)
    public String editRemoveAccount(@PathVariable("operation") String operation,
                                     @PathVariable("userId") Long userId, final RedirectAttributes redirectAttributes,
                                     Model model) {
        if(operation.equals("delete")) {
            if(accountRepository.exists(userId)) {
                accountRepository.delete(userId);
                MessageHelper.addSuccessAttribute(redirectAttributes,  "user.deleted");
            } else {
                MessageHelper.addErrorAttribute(redirectAttributes,  "user.notfound");
            }
        } else if(operation.equals("edit")){
            Account account = accountRepository.findById(userId);
            if(account!=null) {
                model.addAttribute("account", account);
                return SIGNUP_VIEW_NAME;
            } else {
                MessageHelper.addSuccessAttribute(redirectAttributes,  "signup.success");
            }
        }

        return "redirect:/users";
    }




    @RequestMapping(value = "account/current", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    public Account accounts(Principal principal) {
        Assert.notNull(principal);
        return accountRepository.findByUsername(principal.getName());
    }


}
