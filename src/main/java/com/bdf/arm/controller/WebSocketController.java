package com.bdf.arm.controller;

import com.bdf.arm.websocket.CalcInput;
import com.bdf.arm.websocket.Result;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Layout Dialect usage example.
 */
@Controller
//@Secured("ROLE_USER")
class WebSocketController {


    @ModelAttribute("page")
    public String module() {
        return "websocket";
    }

    @RequestMapping(value = "websocket", method = RequestMethod.GET)
    public String messages(Model model) {
        return "websocket/websocket";
    }


    @MessageMapping("/add" )
    @SendTo("/topic/showResult")
    public Result addNum(CalcInput input) throws Exception {
        Thread.sleep(2000);
        Result result = new Result(input.getNum1()+"+"+input.getNum2()+"="+(input.getNum1()+input.getNum2()));
        return result;
    }

}
