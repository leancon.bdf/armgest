package com.bdf.arm.controller;

import com.bdf.arm.entity.Account;
import com.bdf.arm.entity.UserRole;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import java.util.List;

public class SignupForm {

    private static final String NOT_BLANK_MESSAGE = "{notBlank.message}";
    private static final String EMAIL_MESSAGE = "{email.message}";

    @NotBlank(message = SignupForm.NOT_BLANK_MESSAGE)
    @Email(message = SignupForm.EMAIL_MESSAGE)
    private String email;

    @NotBlank(message = SignupForm.NOT_BLANK_MESSAGE)
    private String username;

    @NotBlank(message = SignupForm.NOT_BLANK_MESSAGE)
    private String password;

    @NotBlank(message = SignupForm.NOT_BLANK_MESSAGE)
    private String name;

    @NotBlank(message = SignupForm.NOT_BLANK_MESSAGE)
    private String lastName;

    private String identificationType;

    private String identificationNumber;

    private String address;

    private String addressNumber;

    private Integer cellPhone;

    private Integer cellPhoneAlternative;

    private Integer phone;

    private Integer phoneAlternative;

    private String country;

    private String province;

    private String location;

    private String postalCode;

    private String role;

    private List<UserRole> roles;

    private boolean isWhiteList;

    private boolean isEnabled;

    public List<UserRole> getRoles() {
        return roles;
    }

    public void setRoles(List<UserRole> roles) {
        this.roles = roles;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddressNumber() {
        return addressNumber;
    }

    public void setAddressNumber(String addressNumber) {
        this.addressNumber = addressNumber;
    }

    public Integer getCellPhone() {
        return cellPhone;
    }

    public void setCellPhone(Integer cellPhone) {
        this.cellPhone = cellPhone;
    }

    public Integer getCellPhoneAlternative() {
        return cellPhoneAlternative;
    }

    public void setCellPhoneAlternative(Integer cellPhoneAlternative) {
        this.cellPhoneAlternative = cellPhoneAlternative;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public boolean getIsWhiteList() {
        return isWhiteList;
    }

    public void setIsWhiteList(boolean isWhiteList) {
        this.isWhiteList = isWhiteList;
    }

    public boolean getIsEnabled() {
        return isEnabled;
    }

    public void setIsEnabled(boolean isEnabled) {
        this.isEnabled = isEnabled;
    }

    public String getIdentificationType() {
        return identificationType;
    }

    public void setIdentificationType(String identificationType) {
        this.identificationType = identificationType;
    }

    public String getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    public Integer getPhone() {
        return phone;
    }

    public void setPhone(Integer phone) {
        this.phone = phone;
    }

    public Integer getPhoneAlternative() {
        return phoneAlternative;
    }

    public void setPhoneAlternative(Integer phoneAlternative) {
        this.phoneAlternative = phoneAlternative;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Account createAccount() {
        return new Account(getUsername(), getPassword(), getRole(), getEmail(), getCountry(), getName(),getLastName(),
                getIdentificationType(), getIdentificationNumber(),  getPhone(), getPhoneAlternative(),
                getCellPhone(), getCellPhoneAlternative(), getAddress(), getAddressNumber(),getPostalCode(),
                getLocation(),getProvince(), isEnabled, isWhiteList);
    }



}
