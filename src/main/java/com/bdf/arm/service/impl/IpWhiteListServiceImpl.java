package com.bdf.arm.service.impl;

import com.bdf.arm.entity.IpWhiteList;
import com.bdf.arm.repository.IpWhiteListRepository;
import com.bdf.arm.service.IpWhiteListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by leandro on 12/05/16.
 */
@Component
public class IpWhiteListServiceImpl implements IpWhiteListService {

    @Autowired
    private IpWhiteListRepository repository;

    @Override
    public List<IpWhiteList> findAllEnabled() {
        return repository.findAllEnabled();
    }
}
