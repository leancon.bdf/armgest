package com.bdf.arm.service.impl;

import com.bdf.arm.entity.UserRole;
import com.bdf.arm.repository.UserRoleRepository;
import com.bdf.arm.service.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by leandro on 26/05/16.
 */
@Component
public class UserRoleServiceImpl implements UserRoleService {

    @Autowired
    private UserRoleRepository userRoleRepository;


    @Override
    public List<UserRole> findAll() {
        return userRoleRepository.findAll();
    }
}
