package com.bdf.arm.service;

import com.bdf.arm.entity.IpWhiteList;

import java.util.List;

/**
 * Created by leandro on 12/05/16.
 */
public interface IpWhiteListService {

    List<IpWhiteList> findAllEnabled ();
}
