package com.bdf.arm.service;

import com.bdf.arm.entity.Account;

import java.util.List;

/**
 * Created by leandro on 26/05/16.
 */
public interface AccountService {

    List<Account> findAll();

    Account save(Account account);

    Account findByUsername(String userName);

}
