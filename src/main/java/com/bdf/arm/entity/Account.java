package com.bdf.arm.entity;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import java.util.Collection;
import java.util.Collections;

@SuppressWarnings("serial")
@Entity
@Table(name = "account")
//@NamedQuery(name = Account.FIND_BY_USERNAME, query = "select a from Account a where a.username = :username")
public class Account extends BaseEntityFull implements java.io.Serializable {

   // public static final String FIND_BY_USERNAME = "Account.findByUsername";

    public static final String ROLE_USER = "ROLE_USER";
    public static final String ROLE_ADMIN = "ROLE_ADMIN";

    @Column(unique = true, length = 25)
    private String username;

    @JsonIgnore
    @Column(length = 100)
    private String password;

    @Column(length = 25)
    private String role = ROLE_USER;

    @Column(length = 25)
    private String email;

   // @Column(columnDefinition = "char(2)")
   @Column(length = 25)
   private String country;

    @Column(length = 25)
    private String name;

    @Column(name = "lastname", length = 25)
    private String lastName;

    @Column(name = "identification_type", length = 5)
    private String identificationType;

    @Column(name = "identification_number", length = 25)
    private String identificationNumber;

    @Column(length = 25)
    private Integer phone;

    @Column(name = "phone_alternative", length = 25)
    private Integer phoneAlternative;

    @Column(name = "cellphone", length = 25)
    private Integer cellPhone;

    @Column(name = "cellphone_alternative", length = 25)
    private Integer cellPhoneAlternative;

    @Column(length = 100)
    private String address;

    @Column(length = 25)
    private String addressNumber;

    @Column(name = "postal_code", length = 10)
    private String postalCode;

    @Column(length = 10)
    private String location;

    @Column(length = 25)
    private String province;

    @Column(name = "enabled")
    private boolean isEnabled;

    @Column(name = "white_list")
    private boolean isWhiteList;

    public Account() {
    }

    public Account(String username, String password, String role, String email, String country, String name,
                   String lastName, String identificationType, String identificationNumber, Integer phone,
                   Integer phoneAlternative, Integer cellPhone, Integer cellPhoneAlternative, String address,
                   String addressNumber, String postalCode, String location,
                   String province, boolean isEnabled, boolean isWhiteList) {
        this.username = username;
        this.password = password;
        this.role = role;
        this.email = email;
        this.country = country;
        this.name = name;
        this.lastName = lastName;
        this.identificationType = identificationType;
        this.identificationNumber = identificationNumber;
        this.phone = phone;
        this.phoneAlternative = phoneAlternative;
        this.cellPhone = cellPhone;
        this.cellPhoneAlternative = cellPhoneAlternative;
        this.address = address;
        this.addressNumber = addressNumber;
        this.postalCode = postalCode;
        this.location = location;
        this.province = province;
        this.isEnabled = isEnabled;
        this.isWhiteList = isWhiteList;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singleton(new SimpleGrantedAuthority(getRole()));

    }

    public boolean getIsWhiteList() {
        return isWhiteList;
    }

    public boolean isAdmin() {
        return ROLE_ADMIN.equals(getRole());
    }

    public boolean isWhiteList() {
        return getIsWhiteList();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getIdentificationType() {
        return identificationType;
    }

    public void setIdentificationType(String identificationType) {
        this.identificationType = identificationType;
    }

    public String getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public int getPhoneAlternative() {
        return phoneAlternative;
    }

    public void setPhoneAlternative(int phoneAlternative) {
        this.phoneAlternative = phoneAlternative;
    }

    public int getCellPhone() {
        return cellPhone;
    }

    public void setCellPhone(int cellPhone) {
        this.cellPhone = cellPhone;
    }

    public int getCellPhoneAlternative() {
        return cellPhoneAlternative;
    }

    public void setCellPhoneAlternative(int cellPhoneAlternative) {
        this.cellPhoneAlternative = cellPhoneAlternative;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddressNumber() {
        return addressNumber;
    }

    public void setAddressNumber(String addressNumber) {
        this.addressNumber = addressNumber;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean isEnabled) {
        this.isEnabled = isEnabled;
    }
}
