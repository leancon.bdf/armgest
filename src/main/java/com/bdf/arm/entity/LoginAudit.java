package com.bdf.arm.entity;

import com.bdf.arm.enums.UserStatusEnum;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by leandro on 05/05/16.
 */
@Entity
@Table(name = "login_audit")
public class LoginAudit extends BaseEntity {

    private Long userId;

    @Column(name = "username")
    private String userName;

    private String ip;

    private UserStatusEnum status;

    public LoginAudit() {
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public UserStatusEnum getStatus() {
        return status;
    }

    public void setStatus(UserStatusEnum status) {
        this.status = status;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public LoginAudit(Long userId, String username, String ip, UserStatusEnum status) {
        this.userId = userId;
        this.userName = username;
        this.ip = ip;
        this.status = status;
    }
}
