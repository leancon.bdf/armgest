package com.bdf.arm.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Created by leandro on 26/05/16.
 */
@Entity
@Table(name = "user_role")
public class UserRole extends BaseEntityFull {

    @Column(length = 30)
    public String role;

    @Column(length = 30)
    public String description;

    public UserRole(String role, String description) {
        this.role = role;
        this.description = description;
    }

    public UserRole() {
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
