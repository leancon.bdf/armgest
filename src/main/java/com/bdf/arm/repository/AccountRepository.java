package com.bdf.arm.repository;

import com.bdf.arm.entity.Account;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface AccountRepository extends CrudRepository<Account, Long> {


    List<Account> findAll();

    Account save(Account account);

    Account findByUsername(String userName);

    Account findById(Long id);


}
